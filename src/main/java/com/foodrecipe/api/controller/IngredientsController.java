package com.foodrecipe.api.controller;

import com.foodrecipe.api.entity.Ingredients;
import com.foodrecipe.api.service.IngredientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(path="api/v1/ingredients")
public class IngredientsController {
    private final IngredientService ingredientService;

    @GetMapping
    public ResponseEntity<Iterable<Ingredients>> getAllIngredients(){
        return ResponseEntity.ok(ingredientService.getAllIngredients());
    }

    @GetMapping(path = "/get")
    public ResponseEntity<Ingredients> getIngredient(@RequestParam(name = "id") String ingredientId){
        return ResponseEntity.ok(ingredientService.getIngredient((Long.parseLong(ingredientId))));
    }

    @PostMapping
    public ResponseEntity<String> addIngredient(@RequestBody Ingredients request){
        return ResponseEntity.ok(ingredientService.addIngredient(request));
    }

    @PutMapping
    public ResponseEntity<String> updateIngredient(@RequestBody Ingredients request){
        return ResponseEntity.ok(ingredientService.updateIngredient(request));
    }

    @DeleteMapping
    public ResponseEntity<String> removeIngredient(@RequestParam(name="id") String ingredientsId){
        return ResponseEntity.ok(ingredientService.removeIngredient((Long.parseLong(ingredientsId))));
    }
}
