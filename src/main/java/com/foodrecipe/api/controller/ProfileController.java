package com.foodrecipe.api.controller;

import com.foodrecipe.api.entity.Profile;
import com.foodrecipe.api.service.ProfileService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(path="api/v1/profile")
public class ProfileController {
    private final ProfileService profileService;

    @PutMapping
    public ResponseEntity<Profile> updateProfile(@RequestBody Profile request){
        return ResponseEntity.ok(profileService.updateProfile(request));
    }
}
