package com.foodrecipe.api.controller;

import com.foodrecipe.api.entity.Profile;
import com.foodrecipe.api.entity.Recipe;
import com.foodrecipe.api.service.RecipeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(path="api/v1/recipe")
public class RecipeController {
    private final RecipeService recipeService;

    @GetMapping
    public ResponseEntity<Iterable<Recipe>> getAllRecipe(@RequestParam(name = "userId") String userId){
        return ResponseEntity.ok(recipeService.getAllRecipe(Long.parseLong(userId)));
    }

    @GetMapping(path="/get")
    public ResponseEntity<Recipe> getRecipe(@RequestParam(name = "userId") String userId, @RequestParam(name="recipeId") String recipeId){
        return ResponseEntity.ok(recipeService.getRecipe(Long.parseLong(userId),Long.parseLong(recipeId)));
    }

    @GetMapping(path="/search")
    public ResponseEntity<Iterable<Recipe>> search(@RequestParam(name = "userId") String userId, @RequestParam String keyword){
        return ResponseEntity.ok(recipeService.search(Long.parseLong(userId),keyword));
    }

    @PostMapping
    public ResponseEntity<Recipe> addRecipe(@RequestBody Recipe request){
        return ResponseEntity.ok(recipeService.addRecipe(request));
    }

    @PutMapping
    public ResponseEntity<Recipe> updateRecipe(@RequestBody Recipe request){
        return ResponseEntity.ok(recipeService.updateRecipe(request));
    }

    @DeleteMapping
    public ResponseEntity<String> removeRecipe(@RequestParam(name = "userId") String userId, @RequestParam(name="recipeId") String recipeId){
        return ResponseEntity.ok(recipeService.removeRecipe(Long.parseLong(userId),Long.parseLong(recipeId)));
    }

}
