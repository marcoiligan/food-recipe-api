package com.foodrecipe.api.service;

import com.foodrecipe.api.auth.AuthenticationRequest;
import com.foodrecipe.api.auth.AuthenticationResponse;
import com.foodrecipe.api.auth.RegisterRequest;
import com.foodrecipe.api.config.JwtService;
import com.foodrecipe.api.entity.Profile;
import com.foodrecipe.api.entity.Role;
import com.foodrecipe.api.entity.User;
import com.foodrecipe.api.exception.ApiRequestException;
import com.foodrecipe.api.repository.ProfileRepository;
import com.foodrecipe.api.repository.UserRepository;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository;
    private final ProfileRepository profileRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public String register(RegisterRequest request){
        boolean flag = userRepository.existsByUsername(request.getUsername());
        if(!flag){
            Profile profile = Profile.builder()
                    .fName(request.getProfile().getFName())
                    .lName(request.getProfile().getLName())
                    .mName(request.getProfile().getMName())
                    .build();
            Profile savedProfile = profileRepository.save(profile);
            User user = User.builder()
                    .profile(savedProfile)
                    .username(request.getUsername())
                    .password(passwordEncoder.encode(request.getPassword()))
                    .role(Role.USER)
                    .build();
            User savedUser = userRepository.save(user);
            return "Success";
        }
        throw new ApiRequestException("Username already exists.");
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request, HttpServletResponse response){
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
            User user = userRepository.findByUsername(request.getUsername()).orElseThrow(() -> new ApiRequestException("Invalid user."));
            var jwtToken = jwtService.generateToken(user);

            Cookie cookie = new Cookie("refresh_token",jwtToken.get("refresh_token"));
            cookie.setHttpOnly(true);

            response.addCookie(cookie);

            return AuthenticationResponse.builder()
                    .access_token(jwtToken.get("access_token"))
                    .userId(user.getProfile().getUserId())
                    .build();
        }catch (Exception e){
            throw new ApiRequestException("Invalid username or password.");
        }
    }

    public AuthenticationResponse refreshToken(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        String refresh_token = null;
        AuthenticationResponse authResponse = new AuthenticationResponse();
        if(cookies != null){
            for(Cookie cookie : cookies){
                if(cookie.getName().equals("refresh_token")){
                    refresh_token = cookie.getValue();
                    if(jwtService.isRefreshTokenValid(refresh_token)) {
                        Long userId = Long.parseLong(jwtService.extractUsername(refresh_token));
                        User user = userRepository.findById(userId).orElseThrow(() -> new ApiRequestException("Invalid user"));
                        var jwtToken = jwtService.generateToken(user);
                        cookie.setValue(jwtToken.get("refresh_token"));
                        cookie.setHttpOnly(true);

                        authResponse = AuthenticationResponse.builder()
                                .access_token(jwtToken.get("access_token"))
                                .userId(user.getProfile().getUserId())
                                .build();
                        response.addCookie(cookie);
                    }else{
                        throw new ApiRequestException("Expired Token");
                    }
                    break;
                }
            }
            return authResponse;
        }
        throw new ApiRequestException("No cookies found.");
    }
}
